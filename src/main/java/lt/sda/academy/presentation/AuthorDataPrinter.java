package lt.sda.academy.presentation;

import lt.sda.academy.model.Author;

public class AuthorDataPrinter {

    public static void authorSaveSuccessPrint(Author author) {
        System.out.println("Autorius buvo išsaugotas sėkmingai: ");
        System.out.printf("Id: %s, Vardas: %s, Pavardė: %s%n", author.getId(), author.getName(), author.getFamilyName());
    }
}
