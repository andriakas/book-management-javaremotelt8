package lt.sda.academy;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.model.Author;
import lt.sda.academy.presentation.ApplicationGreeting;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.presentation.ReviewChoice;
import lt.sda.academy.util.HibernateUtil;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;


public class Main {
    public static final SessionFactory SESSION_FACTORY = HibernateUtil.getSessionFactory();
    public static final AuthorService AUTHOR_SERVICE = new AuthorService(SESSION_FACTORY);

    public static void main(String[] args) {
        int choice = 0;
        while (choice != 4) {
            choice = ApplicationGreeting.selectMainMeniuWindow();
            switch (choice) {
                case 1:
                    AuthorChoice.selectAuthorsChoices();
                    int authorChoose = AuthorChoice.selectAuthorsChoices();
                    switch (authorChoose) {
                        case 1:
                            AuthorChoice.printAllAuthorInfo(AUTHOR_SERVICE.getAllAuthors());
                            break;
                        case 2:
                            String searchPhrase = AuthorChoice.findAuthorBySomething();
                            List<Author> findAuthors = AUTHOR_SERVICE.searchAuthorByNameOrSurname(searchPhrase);
                            AuthorChoice.printAllAuthorInfo(findAuthors);
                            break;
                        case 3:
                            Author author = AuthorChoice.createAuthorLines();
                            AUTHOR_SERVICE.createAuthor(author);
                            break;
                        case 4:
                            long authorId = AuthorChoice.insertAuthorIdMessage();
                            Optional<Author> authorToUpdateOpt = AUTHOR_SERVICE.getAuthorById(authorId);
                            if (authorToUpdateOpt.isPresent()) {
                                Author newInfo = AuthorChoice.createAuthorLines();
                                Author authorToUpdate = authorToUpdateOpt.get();
                                authorToUpdate.setName(newInfo.getName());
                                authorToUpdate.setFamilyName(newInfo.getFamilyName());
                                AUTHOR_SERVICE.updateAuthor(authorToUpdate);
                            } else {
                                AuthorChoice.authorWasNotFoundById(authorId);
                            }
                            break;
                        case 5:
                            long authorToDeleteId = AuthorChoice.insertAuthorIdMessage();
                            Optional<Author> authorToDeleteOpt = AUTHOR_SERVICE.getAuthorById(authorToDeleteId);
                            if (authorToDeleteOpt.isPresent()) {
                                AUTHOR_SERVICE.deleteAuthor(authorToDeleteOpt.get());
                            } else {
                                AuthorChoice.authorWasNotFoundById(authorToDeleteId);
                            }
                    }
                    break;
                case 2:
                    BookChoice.selectBookChoice();
                    break;
                case 3:
                    ReviewChoice.selectReviewChoice();
            }
        }
    }
}
